
module.exports = class Product {
    constructor(code, description, manufacturer,
        purchasePrice, salesPrice, quantity, expirationDate,
        iva, productType, uuid, store) {
        this.code = code
        this.description = description
        this.manufacturer = manufacturer
        this.purchasePrice = purchasePrice
        this.salesPrice = salesPrice
        this.quantity = quantity
        this.expirationDate = expirationDate
        this.iva = iva
        this.productType = productType
        this.uuid = uuid
        this.store = store
    }
}
