exports.handleResponse = (result, res) => {
    return result.error ? res.sendStatus(500) : res.sendStatus(200)
}
