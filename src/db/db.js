const { Pool } = require('pg')

const pool = new Pool()

const query = async (text, params) => {
    try {
        const start = Date.now()
        const response = await pool.query(text, params)
        const duration = Date.now() - start
        console.log('executed query', { text, duration, rows: response.rowCount })
        return response.rows
    } catch (err) {
        console.error(err)
        return { error: true }
    }
}

module.exports = {
    query
}
