const express = require('express')
const mountRoutes = require('./routes')
const PORT = process.env.SERVER_PORT
const app = express()

app.use(express.json())
app.use(express.urlencoded({ extended: true }))
mountRoutes(app)

app.listen(PORT, () => {
    console.log(`Genesis API Server running at http://${PORT}`)
})
