// modular router
const express = require('express')
const clientRouter = express.Router()

// every call to this microservice goes through this middleware first
clientRouter.use((req, res, next) => {
    console.log('Time: ', Date.now())
    next()
})

clientRouter.get('/findById/:clientId', (req, res) => {
    res.send({ id: 1, name: 'Genesis Tello Barcia', age: 33 })
})

module.exports = clientRouter
