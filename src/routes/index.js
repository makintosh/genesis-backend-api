const productMs = require('./product.ms')
const clientMs = require('./client.ms')

module.exports = app => {
    app.use('/v1/products', productMs)
    app.use('/client', clientMs)
}
