const express = require('express')
const productRouter = express.Router()
const { getAll, getByFilters, getProductType, remove, save, update } = require('../repository/product.repository')
const { handleResponse } = require('../utils/commons.util')

// every call to this microservice goes through this middleware first
productRouter.use((req, res, next) => {
    console.log('Time: ', Date.now())
    next()
})

productRouter.get('/', async (req, res) => {
    const products = await getAll()
    res.send(products)
})

productRouter.get('/type', async (req, res) => {
    const productType = await getProductType()
    res.send(productType)
})

productRouter.get('/:column/:filter', async (req, res) => {
    const { column, filter } = req.params
    const products = await getByFilters(column, filter)
    products ? res.send(products) : res.sendStatus(400)
})

productRouter.delete('/:code', async (req, res) => {
    console
    const result = await remove(req.params.code)
    handleResponse(result, res)
})

productRouter.post('/', async (req, res) => {
    const result = await save(req.body)
    handleResponse(result, res)
})

productRouter.put('/:code', async (req, res) => {
    const result = await update(req.params.code, req.body)
    handleResponse(result, res)
})

module.exports = productRouter
