const db = require('../db/db')

const COLUMN = {
    NAME: 'producto',
    DESCRIPTION: 'descripcion',
    UUID: 'unic_ode'
}

/**
 * Returns all products sorted by codigo
 * @returns 
 */
exports.getAll = async () => {
    const sqlQuery = 'SELECT unic_ode, descripcion, talla, stock, p_venta, codigo, iva_pro, p_compra, tienda FROM producto_view ORDER BY codigo ASC'
    return await db.query(sqlQuery)
}

/**
 * 
 * @param {*} column
 * @param {*} filter 
 * @returns 
 */
exports.getByFilters = async (column, filter) => {
    const columnSearch = COLUMN[column]
    if (!columnSearch) {
        return null
    }
    const sqlQuery = `SELECT unic_ode, descripcion, talla, stock, p_venta, codigo, iva_pro, p_compra, tienda 
        FROM producto_view WHERE ${columnSearch} LIKE '%${filter}%'`
    return await db.query(sqlQuery)
}

/**
 * Returns data from tipo_producto table
 * @returns 
 */
exports.getProductType = async () => {
    const sqlQuery = 'SELECT * FROM tipo_producto ORDER BY id ASC'
    return await db.query(sqlQuery)
}

exports.remove = async (code) => {
    const sqlQuery = 'DELETE FROM producto WHERE codigo_pro=$1'
    return await db.query(sqlQuery, [code])
}

/**
 * Save a product
 */
exports.save = async (product) => {
    // TODO: set codigo_pro as bigserial(sequential) in BD
    const sqlQuery = `INSERT INTO producto(codigo_pro, descripcion_pro, unidades_pro, iva_pro, 
    precio_venta_pro, precio_compra_pro, id_mar, talla_pro, codigo_identificador_pro, 
    id_tip_pro, tienda_pro, fabricante_pro) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12)`
    return await db.query(sqlQuery, Object.values(product))
}

/**
 * Update a product by codigo_pro
 * @param {*} code
 * @param {*} product 
 * @returns 
 */
exports.update = (code, product) => {
    const sqlQuery = `UPDATE producto SET descripcion_pro='${product.description}', 
        unidades_pro='${product.quantity}',
        iva_pro = '${product.iva}', 
        precio_venta_pro= '${product.pvp}', 
        talla_pro = '${product.talla_pro}', 
        id_tip_pro = '${product.type}',
        id_mar = '${product.id_mar}',
        codigo_identificador_pro = '${product.uuid}', 
        tienda_pro = '${product.store}' WHERE codigo_pro=${code}`
    return db.query(sqlQuery)
}
